﻿$(function () {
    var users = $.connection.users,
        usersContainer = $('.js-users');

    function renderUser(user) {
        usersContainer.append('<li>' + user.Id + '</li>');
    }

    users.client.userConnected = function (data) {
        var user = usersContainer.find('li:contains(' + data.Id + ')');
        if (user.length === 0) {
            renderUser(data);
        }
    };

    users.client.userDisconnected = function (data) {
        usersContainer.find('li:contains(' + data + ')').remove();
    };
    
    $.connection.hub.logging = true;
    $.connection.hub.start().done(function() {
        users.server.getUsers().done(function(users) {
            for (var i = 0; i < users.length; i++) {
                renderUser(users[i]);
            }
        });
    });
});