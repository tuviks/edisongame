﻿(function () {
    var gamesHub = $.connection.games,
        gamesContainer = $('.js-games-container'),
        gameModal = $('.js-new-game-modal'),
        mainContainer = $('.js-main-container'),
        lobby = $('.js-lobby'),
        timer,
        timerId,
        playerMoveDone,
        currentUser = lobby.attr('data-user-name'),
        isPenalty,
        penaltyTimer;

    function userIsOwner(game) {
        return game.owner === currentUser;
    }

    function opponentName(game) {
        return userIsOwner(game) ? game.player : game.owner;
    }

    function getStateText(state) {
        if (state === 1) {
            return 'Waiting for player';
        }
        if (state === 2) {
            return 'Ready to start';
        }
        if (state === 3) {
            return 'Running';
        }
        if (state === 4) {
            return 'Completed';
        }
    }

    function renderGame(game) {
        gamesContainer.append('<tr data-id="' + game.id + '" class="js-game">'
                + '<td class="js-games-list-game-name">' + game.name + '</td>'
                + '<td>' + game.owner + '</td>'
                + '<td>' + game.timeout + '</td>'
                + '<td class="js-game-state ">' + getStateText(game.state) + '</td>'
                + '<td><button class="js-join-game-button"' + (game.state !== 1 ? ' hidden' : '')
                    + (isPenalty ? ' disabled' : '') + '>Join</button></td>'
                + '</<tr>');
    }

    function renderGameHistory(game) {
        var opponentActionContainer = lobby.find('.js-opponent-action'),
            gameHistory = lobby.find('.js-game-history');

        lobby.find('.js-game-help-button-container').hide();
        opponentActionContainer.empty();
        gameHistory.empty();
        if (game.rounds.length > 0) {
            var lastRound = game.rounds[game.rounds.length - 1],
                opponentAction = userIsOwner(game) ? lastRound.playerAction : lastRound.ownerAction;
            for (var i = 0; i < game.rounds.length; i++) {
                var round = game.rounds[i];
                if (round.winner != null) {
                    var roundResult;
                    if (round.winner === 'draw') {
                        roundResult = 'Draw';
                    } else if (round.winner === currentUser) {
                        roundResult = 'Won!';
                    } else {
                        roundResult = 'Lost';
                    }

                    var userUsedHelp = userIsOwner(game) ? round.ownerUsedHelp : round.playerUsedHelp,
                        opponentUsedHelp = userIsOwner(game) ? round.playerUsedHelp : round.ownerUsedHelp;

                    gameHistory.append('<tr><td class="text-right">'
                        + (userUsedHelp ? 'Used help' : '')
                        + '</td><td>' + (i + 1) + ' - '
                        + roundResult
                        + '</td><td class="text-left">'
                        + (opponentUsedHelp ? 'Used help' : '')
                        + '</td></tr>');
                }
            };

            if (lastRound.winner != null) {
                if (opponentAction != null) {
                    if (opponentAction === 0) {
                        $('<div>Timeout</div>').appendTo(opponentActionContainer);
                    }
                    var selectedCard = lobby.find('.js-game-cards [data-card-id=' + opponentAction + ']').clone();
                    selectedCard.appendTo(opponentActionContainer);
                    opponentActionContainer.show();
                }
            }
        }
    }

    function updateGame(game) {
        var gameResult = lobby.find('.js-game-result');
        //if (!lobby.is(":visible")) {
        //    gamesHub.server.closeGame();
        //}
        lobby.find('.js-game-name').html(game.name);
        lobby.find('.js-user-name').html(currentUser);
        lobby.find('.js-opponent-name').html(opponentName(game));

        if (userIsOwner(game) && game.state === 2) {
            lobby.find('.js-start-game-button').show();
        } else {
            lobby.find('.js-start-game-button').hide();
        }
        gameResult.hide();

        renderGameHistory(game);
        clearTimeout(timerId);

        lobby.find('.js-game-user-score').html(userIsOwner(game) ? game.ownerScore : game.playerScore);
        lobby.find('.js-game-opponent-score').html(userIsOwner(game) ? game.playerScore : game.ownerScore);

        if (game.winner !== '') {
            if (game.winner === currentUser) {
                gameResult.html('Congratulations! You won!');
            } else {
                gameResult.html('You loose');
            }
            gameResult.show();
        }
    }

    function playerMove(actionId) {
        if (playerMoveDone === false) {
            playerMoveDone = true;
            var selectedCardContainer = lobby.find('.js-game-selected-card');
            gamesHub.server.playerMove(actionId);

            lobby.find('.js-game-cards').hide();
            selectedCardContainer.empty();

            if (actionId === 0) {
                $('<div>Timeout</div>').appendTo(selectedCardContainer);
            } else {
                var selectedCard = lobby.find('.js-game-cards [data-card-id=' + actionId + ']').clone();
                selectedCard.appendTo(selectedCardContainer);
                clearTimeout(timerId);
            }

            selectedCardContainer.show();
        }
    }

    function timerTick(timeout) {
        lobby.find('.js-game-timeout').html(timeout - timer);
        timer += 1;
        if (timer <= timeout) {
            timerId = setTimeout(function () {
                timerTick(timeout);
            }, 1000);
        } else {
            playerMove(0);
        }
    }

    function showHelp(cardId) {
        var opponentActionContainer = lobby.find('.js-opponent-action');
        opponentActionContainer.empty();
        if (cardId === 0) {
            opponentActionContainer.html('Your help lost. Opponent did not choose the figure.');
        } else {
            opponentActionContainer.html('50% that opponent chose this figure.<br/>');
            var selectedCard = lobby.find('.js-game-cards [data-card-id=' + cardId + ']').clone();
            selectedCard.appendTo(opponentActionContainer);
        }
        lobby.find('.js-game-help-button-container').hide();
        opponentActionContainer.show();
    }

    function startRound(game) {
        lobby.find('.js-game-timeout').html(game.timeout);
        lobby.find('.js-game-selected-card').hide();
        lobby.find('.js-game-cards').show();
        lobby.find('.js-opponent-action').hide();
        lobby.find('.js-game-help-button-container').show();
        timer = 0;
        timerTick(game.timeout);
    }

    function openLobby(gameId) {
        lobby.attr('data-game-id', gameId);
        mainContainer.hide();
        lobby.show();
    }

    function closeLobby() {
        lobby.attr('data-game-id', '');
        mainContainer.show();
        lobby.hide();
        lobby.find('.js-game-cards').hide();
        lobby.find('.js-game-result').hide();
        lobby.find('.js-game-selected-card').hide();
        lobby.find('.js-opponent-action').hide();
        lobby.find('.js-game-timeout').empty();
        lobby.find('.js-game-help-button').show();
        lobby.find('.js-game-help-button-container').hide();
    }

    function penaltyTick(time) {
        setTimeout(function () {
            if (time > penaltyTimer) {
                penaltyTimer += 1000;
                $('.js-penalty-time').html((time - penaltyTimer) / 1000);
                penaltyTick(time);
            } else {
                $('.js-penalty-message').hide();
                mainContainer.find('#newGameButton').prop('disabled', false);
                gameModal.find('.js-new-game-button').prop('disabled', false);
                gamesContainer.find('.js-join-game-button').prop('disabled', false);
                isPenalty = false;
            }
        }, 1000);
    }

    function startPenalty(time) {
        if (time <= 0) {
            return;
        }
        isPenalty = true;
        penaltyTimer = 0;
        mainContainer.find('#newGameButton').prop('disabled', true);
        gameModal.find('.js-new-game-button').prop('disabled', true);
        gamesContainer.find('.js-join-game-button').prop('disabled', true);
        $('.js-penalty-time').html(time / 1000);
        $('.js-penalty-message').show();
        penaltyTick(time);
    }

    $(function () {
        startPenalty($('.js-penalty-message').attr('data-penalty-time'));

        gamesHub.client.gameCreated = function (data) {
            var game = gamesContainer.find('tr[data-id="' + data.id + '"]');
            if (game.length === 0) {
                renderGame(data);
            }
        };

        gamesHub.client.gameClosed = function (data) {
            gamesContainer.find('tr[data-id="' + data.id + '"]').remove();
            if (lobby.attr('data-game-id') === data.id) {
                closeLobby();
            }
        };

        gamesHub.client.gameStateChanged = function (data) {
            var game = gamesContainer.find('tr[data-id="' + data.id + '"]'),
                joinButton = game.find('.js-join-game-button');
            game.find('.js-game-state').html(getStateText(data.state));
            if (data.state === 1) {
                joinButton.show();
            } else {
                joinButton.hide();
            }
        };

        gamesHub.client.updateGame = function (game) {
            updateGame(game);
        };

        gamesHub.client.startRound = function (game) {
            updateGame(game);
            playerMoveDone = false;
            startRound(game);
        };

        gamesHub.client.endRound = function (game) {
            updateGame(game);
            if (currentUser === game.owner) {
                setTimeout(function () {
                    gamesHub.server.startRound();
                }, 3000);
            }
        };

        gamesHub.client.startPenalty = function (time) {
            startPenalty(time);
        };

        $.connection.hub.logging = true;

        $.connection.hub.start(function () {

            gamesHub.server.getGames().done(function (games) {
                for (var i = 0; i < games.length; i++) {
                    renderGame(games[i]);
                }
            });

            gameModal.find('.js-new-game-button').on('click', function () {
                var name = gameModal.find('.js-new-game-name').val(),
                    timeout = gameModal.find('.js-new-game-timeout').val();

                gameModal.modal('hide');
                gamesHub.server.newGame(name, timeout).done(function (game) {
                    updateGame(game);
                    openLobby(game.id);
                });
            });

            lobby.find('#backButton').on('click', function () {
                $('.js-leave-game-alert-modal').modal('show');
            });

            $('.js-leave-game-alert-modal .js-leave-game-modal-button').on('click', function () {
                $('.js-leave-game-alert-modal').modal('hide');
                closeLobby();
                gamesHub.server.closeGame();
            });

            gamesContainer.on('click', '.js-join-game-button', function (e) {
                var button = $(e.currentTarget),
                    gameId = button.closest('.js-game').attr('data-id'),
                    joinGameModal = $('.js-join-game-alert-modal');

                var gameName = button.closest('.js-game').find('.js-games-list-game-name').html();

                joinGameModal.attr('data-game-id', gameId);
                joinGameModal.find('.js-game-name').html(gameName);
                joinGameModal.modal('show');
            });

            $('.js-join-game-alert-modal .js-join-game-modal-button').on('click', function () {
                var joinGameModal = $('.js-join-game-alert-modal');
                joinGameModal.modal('hide');
                gamesHub.server.joinGame(joinGameModal.attr('data-game-id')).done(function (game) {
                    updateGame(game);
                    openLobby(game.id);
                });
            });

            lobby.find('.js-start-game-button').on('click', function () {
                gamesHub.server.startRound();
            });

            lobby.find('.js-game-cards img').on('click', function (e) {
                var cardId = $(e.currentTarget).attr('data-card-id');
                playerMove(cardId);
            });

            lobby.find('.js-game-help-button').on('click', function (e) {
                var button = $(e.currentTarget);
                button.hide();
                gamesHub.server.getHelp().done(function (cardId) {
                    showHelp(cardId);
                });
            });

        }).done(function () {


        });
    });
    
})();