﻿using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;

namespace EdisonGame.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
