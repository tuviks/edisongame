﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EdisonGame.Services
{
    public class ActionService : IActionService
    {
        public int GetWinner(Actions firstAction, Actions secondAction)
        {
            return ActionsTable[(int) firstAction, (int) secondAction];
        }

        private int[,] ActionsTable
        {
            get
            {
                return new int[6, 6]
                {
                    {0, -1, -1, -1, -1, -1},
                    {1, 0, 1, -1, 1, -1},
                    {1, -1, 0, 1, 1, -1},
                    {1, 1, -1, 0, -1, 1},
                    {1, -1, -1, 1, 0, 1},
                    {1, 1, 1, -1, -1, 0}
                };
            }
        }
    }
}
