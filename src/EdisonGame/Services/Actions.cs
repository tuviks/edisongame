﻿namespace EdisonGame.Services
{
    public enum Actions
    {
        Timeout = 0,
        Rock = 1,
        Scissors = 2,
        Paper = 3,
        Lizard = 4,
        Spock = 5
    }
}
