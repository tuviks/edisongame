﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EdisonGame.Services
{
    public interface IActionService
    {
        int GetWinner(Actions firstAction, Actions secondAction);
    }
}
