﻿using EdisonGame.Services;

namespace EdisonGame.Hubs
{
    public class Round
    {
        public Actions? OwnerAction { get; set; }

        public bool OwnerUsedHelp { get; set; }

        public Actions? PlayerAction { get; set; }

        public bool PlayerUsedHelp { get; set; }

        public string Winner { get; set; }
    }
}
