﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using EdisonGame.Services;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace EdisonGame.Hubs
{
    [HubName("games")]
    public class GamesHub : Hub
    {
        private static readonly ConcurrentDictionary<string, Game> _games = new ConcurrentDictionary<string, Game>();
        private static readonly ConcurrentDictionary<string, DateTime> _usersPenalty = new ConcurrentDictionary<string, DateTime>();

        private readonly IActionService _actionService;
        private const int DefaultTimeout = 5;
        private const int PenaltyTime = 60000;
        public static ConcurrentDictionary<string, Game> Games
        {
            get { return _games; }
        }

        public GamesHub(IActionService actionService)
        {
            _actionService = actionService;
        }

        public object NewGame(string name, int timeout)
        {
            var game = new Game
            {
                Id = Context.ConnectionId,
                Name = name,
                Timeout = timeout <= 0 ? DefaultTimeout : timeout,
                Owner = Context.User.Identity.Name,
                State = GameState.WatingPlayer
            };
            _games.GetOrAdd(Context.ConnectionId, game);
            Clients.All.gameCreated(GetGameObj(game));
            return GetFullGameObj(game);
        }

        public IEnumerable<object> GetGames()
        {
            return _games.Select(x => GetGameObj(x.Value));
        }

        public void CloseGame()
        {
            GameClosed(Context.ConnectionId);
        }

        public object JoinGame(string gameId)
        {
            var game = GetGame(gameId);

            if (game == null || game.Player != null)
            {
                return null;
            }

            game.PlayerId = Context.ConnectionId;
            game.Player = Context.User.Identity.Name;
            SetGameState(game, GameState.ReadyToStart);
            Clients.Client(game.Id).updateGame(GetFullGameObj(game));

            return GetFullGameObj(game);
        }

        public void StartRound()
        {
            var game = GetGame(Context.ConnectionId);
            if (game == null || (game.State != GameState.ReadyToStart && game.State != GameState.Running) || string.IsNullOrEmpty(game.Player))
            {
                return;
            }

            if (game.State != GameState.Running)
            {
                SetGameState(game, GameState.Running);
            }

            game.Rounds.Add(new Round());
            var gameObj = GetFullGameObj(game);
            Clients.Client(game.Id).startRound(gameObj);
            Clients.Client(game.PlayerId).startRound(gameObj);
        }

        public int GetHelp()
        {
            var game = GetGame(Context.ConnectionId);
            Round lastRound;
            Actions action;

            if ((game == null 
                    && (game = _games.Values.FirstOrDefault(x => x.PlayerId == Context.ConnectionId)) == null)
                || (lastRound = game.Rounds.LastOrDefault()) == null)
            {
                return 0;
            }

            if (game.Id == Context.ConnectionId && lastRound.PlayerAction.HasValue && !game.Rounds.Any(x => x.OwnerUsedHelp))
            {
                action = lastRound.PlayerAction.Value;
                lastRound.OwnerUsedHelp = true;
            }
            else if (game.PlayerId == Context.ConnectionId && lastRound.OwnerAction.HasValue && !game.Rounds.Any(x => x.PlayerUsedHelp))
            {
                action = lastRound.OwnerAction.Value;
                lastRound.PlayerUsedHelp = true;
            }
            else
            {
                if (game.Id == Context.ConnectionId)
                {
                    lastRound.OwnerUsedHelp = true;
                }
                else
                {
                    lastRound.PlayerUsedHelp = true;
                }
                return 0;
            }

            var rnd = new Random();
            int index = rnd.Next(1, 5 + 3);

            if (index > 5)
            {
                return (int) action;
            }
            return index;
        }

        public void PlayerMove(Actions action)
        {
            var game = GetGame(Context.ConnectionId);
            if (game == null && (game = _games.Values.FirstOrDefault(x => x.PlayerId == Context.ConnectionId)) == null)
            {
                return;
            }
            var playerName = Context.User.Identity.Name;
            var currentRound = game.Rounds.LastOrDefault();

            if (currentRound == null)
            {
                return;
            }

            if (game.Owner == playerName)
            {
                currentRound.OwnerAction = action;
            }
            else
            {
                currentRound.PlayerAction = action;
            }

            if (currentRound.OwnerAction.HasValue && currentRound.PlayerAction.HasValue)
            {
                var winner = _actionService.GetWinner(currentRound.OwnerAction.Value, currentRound.PlayerAction.Value);
                if (winner == 1)
                {
                    currentRound.Winner = game.Owner;
                    game.OwnerScore++;
                }
                else if (winner == -1)
                {
                    currentRound.Winner = game.Player;
                    game.PlayerScore++;
                }
                else
                {
                    currentRound.Winner = "draw";
                }

                if (game.OwnerScore == 3 || game.PlayerScore == 3)
                {
                    SetGameState(game, GameState.Completed);
                    game.Winner = game.OwnerScore == 3 ? game.Owner : game.Player;
                    var completedGameObj = GetFullGameObj(game);
                    Clients.Client(game.Id).updateGame(completedGameObj);
                    Clients.Client(game.PlayerId).updateGame(completedGameObj);
                    return;
                }

                var gameObj = GetFullGameObj(game);
                Clients.Client(game.Id).endRound(gameObj);
                Clients.Client(game.PlayerId).endRound(gameObj);
            }
        }

        public override async Task OnDisconnected(bool stopCalled)
        {
            GameClosed(Context.ConnectionId);
            await base.OnDisconnected(stopCalled);
        }

        public static int GetPenalty(string userName)
        {
            foreach (var penalty in _usersPenalty.Where(x => x.Value.AddMilliseconds(PenaltyTime) < DateTime.Now))
            {
                RemovePenalty(penalty.Key);
            }
            
            return _usersPenalty
                .Where(x => x.Key == userName)
                .Select(x => ((int)(x.Value.AddMilliseconds(PenaltyTime) - DateTime.Now).TotalSeconds) * 1000)
                .FirstOrDefault();
        }

        private void GameClosed(string connectionId)
        {
            var game = GetGame(connectionId);

            if (game == null)
            {
                var jonedGame = _games.Select(x => x.Value).FirstOrDefault(x => x.PlayerId == connectionId);
                if (jonedGame == null)
                {
                    return;
                }
                switch (jonedGame.State)
                {
                    case GameState.ReadyToStart:
                        jonedGame.PlayerId = null;
                        jonedGame.Player = null;
                        SetGameState(jonedGame, GameState.WatingPlayer);
                        Clients.Client(jonedGame.Id).updateGame(GetFullGameObj(jonedGame));
                        break;
                    case GameState.Running:
                        AddPenalty(jonedGame.Player);
                        RunningOrCompletedGameClosed(jonedGame, jonedGame.Id, jonedGame.Owner);
                        break;
                    case GameState.Completed:
                        RunningOrCompletedGameClosed(jonedGame, jonedGame.Id, jonedGame.Owner);
                        break;
                }
                return;
            }

            switch (game.State)
            {
                case GameState.ReadyToStart:
                case GameState.WatingPlayer:
                    RemoveGame(game);
                    break;
                case GameState.Running:
                    AddPenalty(game.Owner);
                    RunningOrCompletedGameClosed(game, game.PlayerId, game.Player);
                    break;
                case GameState.Completed:
                    RunningOrCompletedGameClosed(game, game.PlayerId, game.Player);
                    break;
            }
        }

        private Game GetGame(string connectionId)
        {
            Game game;
            if (!_games.TryGetValue(connectionId, out game))
            {
                return null;
            }
            return game;
        }

        private void RemoveGame(Game game)
        {
            Game ignored;
            _games.TryRemove(game.Id, out ignored);
            
            Clients.All.gameClosed(new { id = game.Id });
        }

        private void RunningOrCompletedGameClosed(Game game, string opponentId, string opponentName)
        {
            if (game.State == GameState.Running)
            {
                SetGameState(game, GameState.Completed);
                game.Winner = opponentName;
                Clients.Client(opponentId).updateGame(GetFullGameObj(game));
            }
            if (game.State == GameState.Completed)
            {
                if (game.OpponentLeft)
                {
                    RemoveGame(game);
                }
                else
                {
                    game.OpponentLeft = true;
                }
            }
        }

        private object GetGameObj(Game game)
        {
            return new { id = game.Id, name = game.Name, timeout = game.Timeout, owner = game.Owner, state = game.State };
        }

        private object GetFullGameObj(Game game)
        {
            var rounds = new List<object>();
            foreach (var round in game.Rounds)
            {
                rounds.Add(new
                {
                    ownerAction = round.OwnerAction,
                    playerAction = round.PlayerAction,
                    winner = round.Winner,
                    ownerUsedHelp = round.OwnerUsedHelp,
                    playerUsedHelp = round.PlayerUsedHelp
                });
            }
            return new
            {
                id = game.Id,
                name = game.Name,
                timeout = game.Timeout,
                owner = game.Owner,
                ownerScore = game.OwnerScore,
                player = game.Player,
                playerScore = game.PlayerScore,
                state = game.State,
                winner = game.Winner,
                rounds = rounds
            };
        }

        private void SetGameState(Game game, GameState state)
        {
            game.State = state;
            Clients.All.gameStateChanged(new { id = game.Id, state = game.State });
        }

        private void AddPenalty(string userName)
        {
            var date = DateTime.Now;
            _usersPenalty.AddOrUpdate(userName, date, (key, oldValue) => date);
            Clients.User(userName).startPenalty(PenaltyTime);
        }

        private static bool RemovePenalty(string userName)
        {
            DateTime ignored;
            return _usersPenalty.TryRemove(userName, out ignored);
        }
    }
}
