﻿using System.Collections;
using System.Collections.Generic;
using EdisonGame.Services;

namespace EdisonGame.Hubs
{
    public class Game
    {
        public Game()
        {
            Rounds = new List<Round>();
            Winner = string.Empty;
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public string Owner { get; set; }

        public int OwnerScore { get; set; }

        public string PlayerId { get; set; }

        public string Player { get; set; }

        public int PlayerScore { get; set; }

        public int Timeout { get; set; }

        public GameState State { get; set; }

        public bool OpponentLeft { get; set; }

        public string Winner { get; set; }

        public List<Round> Rounds { get; set; }
    }
}
