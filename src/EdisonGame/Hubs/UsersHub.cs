﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Extensions.Logging;

namespace EdisonGame.Hubs
{
    [HubName("users")]
    public class UsersHub : Hub
    {
        private static readonly ConcurrentDictionary<string, string> _clients = new ConcurrentDictionary<string, string>();

        public static ConcurrentDictionary<string, string> GetClients
        {
            get { return _clients; }
        }
        
		public override async Task OnConnected()
        {
            string name = Context.User.Identity.Name;
            if (!string.IsNullOrEmpty(name))
            {
                _clients.GetOrAdd(Context.ConnectionId, name);
                Clients.Others.userConnected(new { Id = name });
            }
            await base.OnConnected();
        }

        public override async Task OnReconnected()
        {
            string name = Context.User.Identity.Name;
            if (!string.IsNullOrEmpty(name))
            {
                _clients.GetOrAdd(Context.ConnectionId, name);
                Clients.Others.userConnected(new {Id = name});
            }
            await base.OnReconnected();
        }
        
        public override async Task OnDisconnected(bool stopCalled)
        {
            string ignored;
            var user = GetUser(Context.ConnectionId);
            _clients.TryRemove(Context.ConnectionId, out ignored);

            if (!string.IsNullOrEmpty(user) && _clients.All(x => x.Value != user))
            {
                Clients.Others.userDisconnected(user);
            }
            await base.OnDisconnected(stopCalled);
        }

        public IEnumerable<object> GetUsers()
        {
            return _clients.Select(x => x.Value).Distinct().Select(x => new { Id = x });
        }

        private string GetUser(string connectionId)
        {
            string user;
            if (!_clients.TryGetValue(connectionId, out user))
            {
                return null;
            }
            return user;
        }
    }
}
