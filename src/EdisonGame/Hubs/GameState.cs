﻿namespace EdisonGame.Hubs
{
    public enum GameState
    {
        WatingPlayer = 1,
        ReadyToStart = 2,
        Running = 3,
        Completed = 4
    }
}
